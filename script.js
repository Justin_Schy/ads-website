const images = document.getElementsByClassName("slide");

const nextButton = document.getElementById("next");
const backButton = document.getElementById("back");
let imageIndex = 1;

function changeImage() {
	setInterval(function () {
		toggleActive(imageIndex);
		imageIndex = imageIndex + 1;
		if (imageIndex === 4) {
			imageIndex = 1;
		}
		toggleActive(imageIndex);
	}, 6000);
}

function toggleActive(image) {
	const tempImage = document.getElementById(`img${image}`);
	tempImage.classList.toggle("active");
}
